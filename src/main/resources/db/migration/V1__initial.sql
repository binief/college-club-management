create table users (
  id integer identity PRIMARY KEY,
  fullname varchar(255),
  username varchar (255) not null,
  password varchar (255) not null,
  department varchar (255),
  email varchar (255),
  contactno varchar (15),
  role varchar (10) not null,
  clubid integer
);

create table clubs (
  id integer identity PRIMARY KEY,
  clubname varchar (255) not null,
  leaderid integer
);

create table events (
  id integer identity PRIMARY KEY,
  eventname varchar (255) not null,
  eventdate date,
  clubid integer
);