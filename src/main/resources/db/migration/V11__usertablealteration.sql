alter table users add column studentid int;

ALTER TABLE users
ADD CONSTRAINT FK_users_studentid
    FOREIGN KEY (studentid) 
    REFERENCES users(id);