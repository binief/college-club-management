create table attendancehdr (
  id integer identity PRIMARY KEY,
  attendance date,
  eventid integer,
  approved varchar(1)
);

create table attendancedtl (
  id integer identity PRIMARY KEY,
  memberid integer,
  present varchar(1),
  atthdrid integer
);

ALTER TABLE attendancehdr
ADD CONSTRAINT FK_attendancehdr_eventid
    FOREIGN KEY (eventid) 
    REFERENCES events(id)
    ON DELETE CASCADE;
    
ALTER TABLE attendancedtl
ADD CONSTRAINT FK_attendancedtl_hdrid
    FOREIGN KEY (atthdrid) 
    REFERENCES attendancehdr(id)
    ON DELETE CASCADE;
    
ALTER TABLE attendancedtl
ADD CONSTRAINT FK_attendancedtl_memberid
    FOREIGN KEY (memberid) 
    REFERENCES users(id);
