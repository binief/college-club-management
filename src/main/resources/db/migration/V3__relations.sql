ALTER TABLE users
ADD CONSTRAINT FK_users_clubid
    FOREIGN KEY (clubid) 
    REFERENCES clubs(id);
    
ALTER TABLE events
ADD CONSTRAINT FK_events_clubid
    FOREIGN KEY (clubid) 
    REFERENCES clubs(id);
    
ALTER TABLE clubs
ADD CONSTRAINT FK_clubs_leaderid
    FOREIGN KEY (leaderid) 
    REFERENCES users(id);
    
    
