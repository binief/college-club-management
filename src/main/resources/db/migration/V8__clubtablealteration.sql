alter table clubs add column staff2id integer;

ALTER TABLE clubs
ADD CONSTRAINT FK_clubs_staff2id
    FOREIGN KEY (staff2id) 
    REFERENCES users(id);