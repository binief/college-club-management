alter table clubs add column staffid integer;

ALTER TABLE clubs
ADD CONSTRAINT FK_clubs_staffid
    FOREIGN KEY (staffid) 
    REFERENCES users(id);