create table memberclubmapping (
  id integer identity PRIMARY KEY,
  clubid integer,
  memberid integer
);

ALTER TABLE memberclubmapping
ADD CONSTRAINT FK_memberclubmapping_clubid
    FOREIGN KEY (clubid) 
    REFERENCES clubs(id);    
    
ALTER TABLE memberclubmapping
ADD CONSTRAINT FK_memberclubmapping_memberid
    FOREIGN KEY (memberid) 
    REFERENCES users(id);
