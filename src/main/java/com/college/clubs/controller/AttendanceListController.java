package com.college.clubs.controller;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.AttendanceHdr;
import com.college.clubs.service.AttendanceService;
import com.college.clubs.util.Utils;

@Scope(value = "session")
@Component(value = "attendancelist")
@ELBeanName(value = "attendancelist")
@Join(path = "/attendances", to = "/attendancelist.jsf")
public class AttendanceListController {
	private List<AttendanceHdr> data;
	private String searchString;
	private String role = "";
	private Integer userId = 0;

	@Autowired
	AttendanceService attService;

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		this.setRole((String) session.getAttribute("role"));
		this.setUserId((Integer) session.getAttribute("userid"));
		loadData();
	}

	private void loadData() {
		if (role != null && role.equals("STAFF")) {
			data = attService.findByStaffId(this.getUserId());
		} else {
			data = attService.getAllAttHdr();
		}
	}

	public List<AttendanceHdr> getData() {
		return data;
	}

	public void setData(List<AttendanceHdr> data) {
		this.data = data;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
