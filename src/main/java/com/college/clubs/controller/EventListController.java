package com.college.clubs.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.college.clubs.model.Events;
import com.college.clubs.service.EventService;
import com.college.clubs.util.Utils;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;

@Scope(value = "session")
@Component(value = "eventlist")
@ELBeanName(value = "eventlist")
@Join(path = "/events", to = "/eventlist.jsf")
public class EventListController {

	private List<Events> data;
	private String searchString;
	private String role = "";
	private Integer userId = 0;
	private Integer studentId = null;

	@Autowired
	EventService eventService;

	@PostConstruct
	public void loadUsers() {

	}

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		this.setRole((String) session.getAttribute("role"));
		this.setUserId((Integer) session.getAttribute("userid"));
		this.setStudentId((Integer) session.getAttribute("studentid"));
		loadData();

	}

	private void loadData() {
		if (role != null && role.equals("LEADER")) {
			data = eventService.getByLeader(userId);
		} else if (role != null && role.equals("PARENT")
				&& this.getStudentId() != null) {
			data = eventService.getByStudent(this.getStudentId());
		} else {
			data = eventService.findAll();
		}
	}

	public void doSearch() {
		if (searchString != null && searchString.trim().length() > 0)
			data = eventService.doSearch(searchString.toLowerCase());
		else
			data = eventService.findAll();
	}

	public void preProcessPDF(Object document) {
		Document doc = (Document) document;
		doc.setPageSize(PageSize.A4.rotate());
	}

	public void printReport(Events e) {
		FileInputStream fis = null;
		FileOutputStream os = null;
		try {
			File file = ResourceUtils.getFile("classpath:eventreport.docx");
			fis = new FileInputStream(file);
			XWPFDocument document = new XWPFDocument(fis);

			List<XWPFParagraph> paragraphs = document.getParagraphs();

			for (int x = 0; x < paragraphs.size(); x++) {
				XWPFParagraph paragraph = paragraphs.get(x);

				List<XWPFRun> xwpfRuns = paragraph.getRuns();
				for (XWPFRun xwpfRun : xwpfRuns) {
					String xwpfRunText = xwpfRun
							.getText(xwpfRun.getTextPosition());
//					System.out.println("xwpfRunText: " + xwpfRunText);
//	                System.out.println("Key: " + key);
					if (xwpfRunText != null
							&& xwpfRunText.contains("eventname")) {
						xwpfRunText = xwpfRunText.replaceAll("eventname",
								e.getEventName());
						xwpfRun.setText(xwpfRunText, 0);
					} else if (xwpfRunText != null
							&& xwpfRunText.contains("eventdate")
							&& e.getEventDate() != null) {

						String pattern = "dd-MM-yyyy h:mm a";
						SimpleDateFormat sdf = new SimpleDateFormat(pattern);
						sdf.setTimeZone(TimeZone.getTimeZone("IST"));

						xwpfRunText = xwpfRunText.replaceAll("eventdate",
								sdf.format(e.getEventDate()));
						xwpfRun.setText(xwpfRunText, 0);
					} else if (xwpfRunText != null
							&& xwpfRunText.contains("venue")) {
						xwpfRunText = xwpfRunText.replaceAll("venue",
								e.getVenue());
						xwpfRun.setText(xwpfRunText, 0);
					} else if (xwpfRunText != null
							&& xwpfRunText.contains("club")) {
						xwpfRunText = xwpfRunText.replaceAll("club",
								e.getClub().getClubName());
						xwpfRun.setText(xwpfRunText, 0);
					} else if (xwpfRunText != null
							&& xwpfRunText.contains("status")) {
						xwpfRunText = xwpfRunText.replaceAll("status",
								e.getStatus());
						xwpfRun.setText(xwpfRunText, 0);
					} else if (xwpfRunText != null
							&& xwpfRunText.contains("details")) {
						xwpfRunText = xwpfRunText.replaceAll("details",
								e.getDescription());
						xwpfRun.setText(xwpfRunText, 0);
					}
				}
			}

			os = new FileOutputStream(System.getenv("APPDATA")
					+ "\\ClubsDatabase\\eventreport.docx");
			document.write(os);

			convertToPdf(
					System.getenv("APPDATA")
							+ "\\ClubsDatabase\\eventreport.docx",
					System.getenv("APPDATA")
							+ "\\ClubsDatabase\\eventreport.pdf");

//			openPdf(System.getenv("APPDATA")
//					+ "\\ClubsDatabase\\eventreport.pdf");

			Utils.redirectToUrl("/eventreport?eventid=" + e.getId());

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
				if (os != null) {
					os.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private int convertToPdf(String source, String dest) {
		File file = null;
		try {
			Process process;
			file = ResourceUtils.getFile("classpath:OfficeToPDF.exe");
			process = new ProcessBuilder(file.getAbsolutePath(), source, dest)
					.start();
			process.waitFor();

			return process.exitValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public List<Events> getData() {
		return data;
	}

	public void setData(List<Events> data) {
		this.data = data;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

}
