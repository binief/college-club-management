package com.college.clubs.controller;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.AttendanceDtl;
import com.college.clubs.model.AttendanceHdr;
import com.college.clubs.model.Users;
import com.college.clubs.service.AttendanceService;
import com.college.clubs.service.MailService;
import com.college.clubs.service.UserService;
import com.college.clubs.util.Utils;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;

@Scope(value = "session")
@Component(value = "attendance")
@ELBeanName(value = "attendance")
@Join(path = "/attendance", to = "/attendance.jsf")
public class AttendanceController {

	private Integer attendanceId;
	private AttendanceHdr attHdr;
	private List<AttendanceDtl> attDtl;
	private List<AttendanceDtl> presentDtl;
	private String role;

	@Autowired
	AttendanceService attService;

	@Autowired
	MailService mailService;

	@Autowired
	UserService userService;

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		role = (String) session.getAttribute("role");
		loadData();
	}

	private void loadData() {
		if (attendanceId != null && attendanceId > 0) {
			attHdr = attService.getById(attendanceId);
			attDtl = attService.getDetailByHdrId(attHdr.getId());
			presentDtl = attService.getPresentDetailByHdrId(attHdr.getId());
		}
	}

	public void saveAttendance() {
		try {
			for (AttendanceDtl att : attDtl) {
				attService.saveDtl(att);
			}

			sendMail();
			Utils.redirectToUrl("/attendances");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendMail() {
		try {
			List<Users> princi = userService.findByRole("PRINCIPAL");
			for (Users u : princi) {
				String subject = "Attendance Approval | Request";
				String msg = "Dear Sir," + " \n"
						+ "Please approve the attendance for the event "
						+ attHdr.getEvent().getEventName() + "\n" + "Thank You";
				mailService.sendMail(subject, msg, u.getEmail());
			}
		} catch (Exception e) {
		}
	}

	public void approveAttendance() {
		attHdr.setApproved("A");
		attHdr = attService.saveHdr(attHdr);

		for (AttendanceDtl att : attDtl) {
			attService.saveDtl(att);
		}

		List<Users> princi = userService.findByRole("STAFF");
		for (Users u : princi) {
			String subject = "Attendance Approved";
			String msg = "Dear Sir," + " \n" + "Attendace for the event "
					+ attHdr.getEvent().getEventName() + " is approved\n"
					+ "Thank You";
			try {
				mailService.sendMail(subject, msg, u.getEmail());
			} catch (Exception e) {
			}
		}

		Utils.redirectToUrl("/attendances");
	}

	public void preProcessPDF(Object document) {
		Document doc = (Document) document;
		doc.setPageSize(PageSize.A4.rotate());
	}

	public String getAttendanceString(AttendanceDtl dtl) {
		if (dtl.getPresent().equals("P")) {
			return "Present";
		} else if (dtl.getPresent().equals("A")) {
			return "Absent";
		} else if (dtl.getPresent().equals("D")) {
			return "Disagree";
		}
		return "";
	}

	public Integer getAttendanceId() {
		return attendanceId;
	}

	public void setAttendanceId(Integer attendanceId) {
		this.attendanceId = attendanceId;
	}

	public AttendanceHdr getAttHdr() {
		return attHdr;
	}

	public void setAttHdr(AttendanceHdr attHdr) {
		this.attHdr = attHdr;
	}

	public List<AttendanceDtl> getAttDtl() {
		return attDtl;
	}

	public void setAttDtl(List<AttendanceDtl> attDtl) {
		this.attDtl = attDtl;
	}

	public List<AttendanceDtl> getPresentDtl() {
		return presentDtl;
	}

	public void setPresentDtl(List<AttendanceDtl> presentDtl) {
		this.presentDtl = presentDtl;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
