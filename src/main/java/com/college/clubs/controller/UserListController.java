package com.college.clubs.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.model.Users;
import com.college.clubs.service.ClubService;
import com.college.clubs.service.MemberClubMappingService;
import com.college.clubs.service.UserService;
import com.college.clubs.util.Utils;

@Scope(value = "session")
@Component(value = "userlist")
@ELBeanName(value = "userlist")
@Join(path = "/users", to = "/userlist.jsf")
public class UserListController {

	private List<Users> users;
	private String searchString;
	private String role;
	private Integer userId;

	@Autowired
	UserService userService;

	@Autowired
	MemberClubMappingService memberService;

	@Autowired
	ClubService clubService;

	@PostConstruct
	public void loadUsers() {

	}

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		role = (String) session.getAttribute("role");
		userId = (Integer) session.getAttribute("userid");
		if (role != null && role.equals("ADMIN")) {
			users = userService.findAll();
		} else if (role != null && role.equals("LEADER") && userId != null) {
			List<Clubs> myclubs = clubService.getClubByLeader(userId);
			
			if (!myclubs.isEmpty()) {
				users = new ArrayList<Users>();
				for (Clubs c : myclubs) {
					List<MemberClubMapping> meCl = memberService
							.getMembersByClub(c.getId());
					List<Users> temp = meCl.stream()
							.filter(i->i.getMember().getRole().equals("MEMBER"))
							.map(MemberClubMapping::getMember)
							.collect(Collectors.toList());
					users.addAll(temp);
				}
			}
		}
	}

	public void searchUser() {
		if (searchString != null && searchString.trim().length() > 0)
			users = userService.searchUser(searchString);
		else
			users = userService.findAll();
	}

	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
