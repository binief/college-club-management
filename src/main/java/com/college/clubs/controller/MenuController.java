package com.college.clubs.controller;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.util.Utils;

@Scope(value = "session")
@Component(value = "menu")
@ELBeanName(value = "menu")
public class MenuController {

	private String pageUrl;

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
	}

	public void changePage(int itemSelected) throws Exception {
		if (itemSelected == 1) {
			Utils.redirectToUrl("/users");
		}
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

}
