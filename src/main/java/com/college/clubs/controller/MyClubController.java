package com.college.clubs.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.model.Users;
import com.college.clubs.service.ClubService;
import com.college.clubs.service.MemberClubMappingService;
import com.college.clubs.service.UserService;
import com.college.clubs.util.Utils;

@Scope(value = "session")
@Component(value = "myclubs")
@ELBeanName(value = "myclubs")
@Join(path = "/myclubs", to = "/myclubs.jsf")

public class MyClubController {
	private List<Clubs> clubs;
	private List<MemberClubMapping> memberclubs;
	private Integer userId = null;
	private String role = null;

	@Autowired
	MemberClubMappingService mappingService;

	@Autowired
	UserService userService;

	@Autowired
	ClubService clubService;

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		Utils.validateSession(session);
		userId = (Integer) session.getAttribute("userid");
		this.setRole((String) session.getAttribute("role"));
		loadData();
	}

	private void loadData() {
		try {
			memberclubs = mappingService.getMyClubs(getUserId());

			List<Integer> myClubIds = memberclubs.stream().map(MemberClubMapping::getClubId)
					.collect(Collectors.toList());

			clubs = clubService.findClubsNotInIds(myClubIds);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void becomeMember(Integer clubId) {
		try {

			Users u = userService.findById(userId);
			if (!u.getStatus()) {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!!", "Your membership expired"));
				throw new Exception("Your membership expired");
			}
			MemberClubMapping m = new MemberClubMapping();
			m.setClubId(clubId);
			m.setMemberId(userId);
			mappingService.becomeMember(m);
			Utils.redirectToUrl("/myclubs");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void leaveMembership(MemberClubMapping m) {
		try {
			mappingService.leaveMember(m);
			Utils.redirectToUrl("/myclubs");
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!!", e.getMessage()));
		}
	}

	public List<Clubs> getClubs() {
		return clubs;
	}

	public void setClubs(List<Clubs> clubs) {
		this.clubs = clubs;
	}

	public List<MemberClubMapping> getMemberclubs() {
		return memberclubs;
	}

	public void setMemberclubs(List<MemberClubMapping> memberclubs) {
		this.memberclubs = memberclubs;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
