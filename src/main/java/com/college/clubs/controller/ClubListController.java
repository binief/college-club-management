package com.college.clubs.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.service.ClubService;
import com.college.clubs.service.MemberClubMappingService;
import com.college.clubs.util.Utils;

@Scope(value = "session")
@Component(value = "clublist")
@ELBeanName(value = "clublist")
@Join(path = "/clubs", to = "/clublist.jsf")
public class ClubListController {

	private List<Clubs> data;
	private String searchString;
	private String role;
	private Integer studentId = null;

	@Autowired
	ClubService clubService;

	@Autowired
	MemberClubMappingService memberService;

	@PostConstruct
	public void loadUsers() {

	}

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		role = (String) session.getAttribute("role");
		Integer userId = (Integer) session.getAttribute("userid");
		this.setStudentId((Integer) session.getAttribute("studentid"));

		if (role != null && role.equals("ADMIN"))
			data = clubService.findAll();
		else if (role != null && role.equals("STAFF")) {
			data = clubService.getClubByStaff(userId);
		} else if (role != null && role.equals("LEADER")) {
			data = clubService.getClubByLeader(userId);
		} else if (role != null && role.equals("PARENT")
				&& this.getStudentId() != null) {
			data = clubService.getByStudent(this.getStudentId());
		} else if (userId != null) {
			List<MemberClubMapping> meCl = memberService
					.getMyClubs((Integer) session.getAttribute("userid"));
			data = meCl.stream().map(MemberClubMapping::getClub)
					.collect(Collectors.toList());
		}

	}

	public void doSearch() {
		if (searchString != null && searchString.trim().length() > 0)
			data = clubService.doSearch(searchString.toLowerCase());
		else
			data = clubService.findAll();
	}

	public List<Clubs> getData() {
		return data;
	}

	public void setData(List<Clubs> data) {
		this.data = data;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

}
