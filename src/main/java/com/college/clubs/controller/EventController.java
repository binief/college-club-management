package com.college.clubs.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.Events;
import com.college.clubs.service.ClubService;
import com.college.clubs.service.EventService;
import com.college.clubs.util.Utils;

@Scope(value = "session")
@Component(value = "event")
@ELBeanName(value = "event")
@Join(path = "/event", to = "/event.jsf")
public class EventController {

	private Integer eventId;
	private Events u;
	private List<Clubs> clubs;
	private String role;
	private Integer userId;
	private Date minDate = new Date();

	@Autowired
	ClubService clubService;

	@Autowired
	EventService eventService;

	@PostConstruct
	public void init() {
	}

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		role = (String) session.getAttribute("role");
		userId = (Integer) session.getAttribute("userid");
		loadClubs();
		loadEvent();
	}

	public void loadEvent() {
		if (eventId > 0) {
			u = eventService.findById(eventId);
		} else {
			u = new Events();
		}
	}

	public void loadClubs() {
		if (role != null && role.equals("LEADER")) {
			clubs = clubService.getClubByLeader(userId);
		} else {
			clubs = clubService.findAll();
		}
	}

	public void saveEvent() {
		try {
			validate(u);

			u = eventService.saveEvent(u);
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Event saved successfully !!!"));
			Utils.redirectToUrl("/events");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Sticky Message", e.getMessage()));
		}

	}

	public void deleteEvent() {
		try {
			eventService.deleteEvent(u);
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Event deleted"));
			Utils.redirectToUrl("/events");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Sticky Message", e.getMessage()));
		}

	}

	private void validate(Events u) {

	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Events getU() {
		return u;
	}

	public void setU(Events u) {
		this.u = u;
	}

	public List<Clubs> getClubs() {
		return clubs;
	}

	public void setClubs(List<Clubs> clubs) {
		this.clubs = clubs;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getMinDate() {
		return minDate;
	}

	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}

}
