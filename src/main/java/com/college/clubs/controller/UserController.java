package com.college.clubs.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.model.Users;
import com.college.clubs.service.ClubService;
import com.college.clubs.service.MemberClubMappingService;
import com.college.clubs.service.UserService;
import com.college.clubs.util.Utils;

@ViewScoped
@Component(value = "user")
@ELBeanName(value = "user")
@Join(path = "/user", to = "/user.jsf")
public class UserController {

	private Integer userId;
	private String createRole;
	private Users u;
	private String view;
	private String role;
	private Integer leaderId;

	@Autowired
	UserService userService;

	@Autowired
	ClubService clubService;

	@Autowired
	MemberClubMappingService mapService;

	@PostConstruct
	public void init() {
	}

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		this.setRole((String) session.getAttribute("role"));
		this.setLeaderId((Integer) session.getAttribute("userid"));
		loadUser();
	}

	public void loadUser() {
		if (userId > 0) {
			u = userService.findById(userId);
		} else {
			u = new Users();
			if (createRole != null) {
				if (createRole.equals("member")) {
					u.setRole("MEMBER");
				}
			}
		}
	}

	private void isDuplicate(boolean newUser) throws Exception {
		String validationMessage = userService.isDuplicateUser(u, newUser);
		if (validationMessage != null) {
			throw new Exception(validationMessage);
		}
	}

	public void saveUser() {
		try {
			boolean newUser = u.getId() == null;

			isDuplicate(newUser);

			u = userService.saveUser(u);
			if (role.equals("LEADER") && newUser) {
				List<Clubs> clubs = clubService.getClubByLeader(leaderId);
				for (Clubs c : clubs) {
					MemberClubMapping m = new MemberClubMapping();
					m.setMember(u);
					m.setClub(c);
					mapService.becomeMember(m);
				}

			}
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Sticky Message", "Message Content"));
			Utils.redirectToUrl("/users");
		} catch (Exception e) {
			if (e.getMessage().contains("USERS_USERNAME_INDEX")) {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"User already found",
								"This user is already found"));
			} else {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Error!!!", e.getMessage()));
			}
		}

	}

	public void deleteUser() {
		try {
			userService.deleteUser(u);
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							"Sticky Message", "Message Content"));
			Utils.redirectToUrl("/users");
		} catch (Exception e) {
			if (e.getMessage().contains("FOREIGN KEY")) {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
								"Cannot delete this user. This user has data !!!"));
			}
		}
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCreateRole() {
		return createRole;
	}

	public void setCreateRole(String createRole) {
		this.createRole = createRole;
	}

	public Users getU() {
		return u;
	}

	public void setU(Users u) {
		this.u = u;
	}

	public void roleChange() {
		if (this.getU().getRole().equals("PRINCIPAL")) {
			this.getU().setDepartment(null);
		}
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getLeaderId() {
		return leaderId;
	}

	public void setLeaderId(Integer leaderId) {
		this.leaderId = leaderId;
	}

}
