package com.college.clubs.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.model.Users;
import com.college.clubs.service.ClubService;
import com.college.clubs.service.MemberClubMappingService;
import com.college.clubs.service.UserService;
import com.college.clubs.util.Utils;

@ViewScoped
@Component(value = "club")
@ELBeanName(value = "club")
@Join(path = "/club", to = "/club.jsf")
public class ClubController {

	private Integer clubId;
	private Clubs u;
	private List<Users> users;
	private List<Users> leaders;
	private List<Users> staffs;
	private List<Users> members;
	private String role;
	private Integer userId;

	@Autowired
	ClubService clubService;

	@Autowired
	UserService userService;

	@Autowired
	MemberClubMappingService mappingService;

	@PostConstruct
	public void init() {
	}

	public void onPageLoad() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		Utils.validateSession(session);
		role = (String) session.getAttribute("role");
		userId = (Integer) session.getAttribute("userid");
		loadClubs();
		loadUsers();
	}

	public void loadClubs() {
		if (clubId > 0) {
			u = clubService.findById(clubId);
		} else {
			u = new Clubs();
		}
	}

	public void loadUsers() {
		users = userService.findAll();

		if (role != null && (role.equals("ADMIN") || role.equals("STAFF"))) {
			leaders = users.stream().filter(i -> i.getRole().equals("LEADER"))
					.collect(Collectors.toList());
		} else if (role != null && role.equals("LEADER") && userId != null) {
			leaders = users.stream().filter(
					i -> i.getRole().equals("LEADER") && i.getId() == userId)
					.collect(Collectors.toList());
		}

		if (u != null) {
			List<MemberClubMapping> mapping = mappingService
					.getMembersByClub(u.getId());

			if (u.getLeader() != null) {
				mapping = mapping.stream()
						.filter(i -> !i.getMember().getId()
								.equals(u.getLeader().getId()))
						.collect(Collectors.toList());
			}

			members = mapping.stream().map(MemberClubMapping::getMember)
					.collect(Collectors.toList());
		}

		staffs = users.stream().filter(i -> i.getRole().equals("STAFF"))
				.collect(Collectors.toList());
	}

	public void saveClub() {
		try {
			clubService.validate(u);
			u = clubService.saveClub(u);
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Club saved successfully !!!"));
			Utils.redirectToUrl("/clubs");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
							e.getMessage()));
		}

	}

	public void deleteClub() {
		try {
			clubService.deleteClub(u);
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Club deleted"));
			Utils.redirectToUrl("/clubs");
		} catch (Exception e) {
			if (e.getMessage().contains("FOREIGN KEY")) {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
								"Cannot delete this club !!!"));
			}
		}
	}

	public void makeLeader(Users user) {
		try {
			clubService.makeLeader(user, u, u.getLeader());

			Utils.redirectToUrl("/clubs");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
							e.getMessage()));
		}
	}

	public Integer getClubId() {
		return clubId;
	}

	public void setClubId(Integer clubId) {
		this.clubId = clubId;
	}

	public Clubs getU() {
		return u;
	}

	public void setU(Clubs u) {
		this.u = u;
	}

	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

	public List<Users> getLeaders() {
		return leaders;
	}

	public void setLeaders(List<Users> leaders) {
		this.leaders = leaders;
	}

	public List<Users> getStaffs() {
		return staffs;
	}

	public void setStaffs(List<Users> staffs) {
		this.staffs = staffs;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<Users> getMembers() {
		return members;
	}

	public void setMembers(List<Users> members) {
		this.members = members;
	}

}
