package com.college.clubs.controller;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Users;
import com.college.clubs.service.UserService;

@Scope("request")
@Component(value = "registration")
@ELBeanName(value = "registration")
@Join(path = "/registration", to = "/registration.jsf")
public class RegistrationController {

	private String fullname;
	private String email;
	private String contact;
	private String username;
	private String password;
	private String confirmpassword;
	private String parentname;
	private String parentemail;
	private Date dateofadmission;
	private String rollno;
	private String program;
	private String department;

	@Autowired
	UserService userService;

	// Create a global, private member for storing the session data...
	private HttpSession session;

	@PostConstruct
	public void initialiseSession() {
		// Assign the session to the global member…
		session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
	}

	public String doRegister() {
		try {
			if (!getPassword().equals(getConfirmpassword())) {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Password Mismatch",
								"Password is not matching"));
				return null;
			}

			Users u = new Users();

			u.setFullname(fullname);
			u.setContactno(contact);
			u.setEmail(email);
			u.setUsername(username);
			u.setPassword(password);
			u.setRole("MEMBER");
			u.setStatus(true);
			u.setRollno(rollno);
			u.setDateofadmission(dateofadmission);
			u.setProgram(program);
			u.setDepartment(department);
			String usrnm = generateRandomString();

			String validationMessage = userService.isDuplicateUser(u, true);
			if (validationMessage != null) {
				throw new Exception(validationMessage);
			}

			u = userService.saveUser(u);

			Users parent = new Users();
			parent.setUsername(usrnm);
			parent.setPassword(generateRandomString());
			parent.setRole("PARENT");
			parent.setStatus(true);
			parent.setFullname(parentname);
			parent.setEmail(parentemail);
			parent.setStudent(u);

			parent = userService.saveUser(parent);

			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Registered Successfully !!!"));

			return "login.xhtml?faces-redirect=true";

		} catch (Exception e) {
			if (e.getMessage().contains("USERS_USERNAME_INDEX")) {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Username already taken",
								"This username is already used"));
			} else {
				FacesContext.getCurrentInstance().addMessage("growl",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Registration Failed", e.getMessage()));
			}
		}
		return null;
	}

	private String generateRandomString() {
		String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		int count = 10;
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random()
					* ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public String getParentname() {
		return parentname;
	}

	public void setParentname(String parentname) {
		this.parentname = parentname;
	}

	public String getParentemail() {
		return parentemail;
	}

	public void setParentemail(String parentemail) {
		this.parentemail = parentemail;
	}

	public Date getDateofadmission() {
		return dateofadmission;
	}

	public void setDateofadmission(Date dateofadmission) {
		this.dateofadmission = dateofadmission;
	}

	public String getRollno() {
		return rollno;
	}

	public void setRollno(String rollno) {
		this.rollno = rollno;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

}
