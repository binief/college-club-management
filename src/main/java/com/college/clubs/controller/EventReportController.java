package com.college.clubs.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.stereotype.Component;

import com.college.clubs.util.Utils;

@ViewScoped
@Component(value = "eventreport")
@ELBeanName(value = "eventreport")
@Join(path = "/eventreport", to = "/eventreport.jsf")
public class EventReportController {

	private StreamedContent streamedContent;
	private String idFile;

	@SuppressWarnings("deprecation")
	public void onPageLoad() {
		InputStream in = null;
		try {
			HttpSession session1 = (HttpSession) FacesContext
					.getCurrentInstance().getExternalContext().getSession(true);
			Utils.validateSession(session1);

			File f = new File(System.getenv("APPDATA")
					+ "\\ClubsDatabase\\eventreport.pdf");
			in = new FileInputStream(f);

			streamedContent = new DefaultStreamedContent(in, "application/pdf");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	public StreamedContent getStreamedContent() {
		if (FacesContext.getCurrentInstance().getRenderResponse()) {
			return new DefaultStreamedContent();
		} else {
			return streamedContent;
		}
	}

	public void setStreamedContent(StreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}

	public String getIdFile() {
		return idFile;
	}

	public void setIdFile(String idFile) {
		this.idFile = idFile;
	}

}
