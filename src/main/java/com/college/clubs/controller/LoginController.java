package com.college.clubs.controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.college.clubs.model.Users;
import com.college.clubs.service.UserService;

@Scope(value = "session")
@Component(value = "login")
@ELBeanName(value = "login")
@Join(path = "/", to = "/login.jsf")
public class LoginController {

	private String username;
	private String password;

	@Autowired
	UserService userService;

	// Create a global, private member for storing the session data...
	private HttpSession session;

	@PostConstruct
	public void initialiseSession() {
		// Assign the session to the global member…
		session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
	}

	public String checkValidUserByEmail() {
		Users u = userService.validUser(this.getUsername(), this.getPassword());

		if (u != null) {
			session.setAttribute("username", u.getUsername());
			session.setAttribute("role", u.getRole());
			session.setAttribute("userid", u.getId());
			session.setAttribute("fullname", u.getFullname());
			session.setAttribute("studentid",
					u.getStudent() != null ? u.getStudent().getId() : null);
			return "home.xhtml" + "?faces-redirect=true";
		} else {
			FacesContext.getCurrentInstance().addMessage("growl",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Unauthorized", "Invalid Credentials !!!"));
			return null;
		}
	}
	
	public String goToRegistration() {
		return "registration.xhtml" + "?faces-redirect=true";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

}
