package com.college.clubs.util;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class Utils {
	public static void redirectToUrl(String url){
		try {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			context.redirect(context.getRequestContextPath() + url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void validateSession(HttpSession session) throws Exception {
		if (session.getAttribute("username") == null) {
			redirectToUrl("/");
		}
	}
}
