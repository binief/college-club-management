package com.college.clubs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.Users;

@Repository
public interface UsersRepo extends JpaRepository<Users, Integer> {

	public List<Users> findByUsernameAndPassword(String username,
			String password);

	public List<Users> findByFullnameIgnoreCase(String fullName);

	@Query("select u from Users u where u.club=:club and (u.role='MEMBER' or u.role='LEADER')")
	public List<Users> getClubMembers(@Param("club") Clubs club);

	public List<Users> findByRole(String role);

	public List<Users> findByUsernameIgnoreCase(String username);

	public List<Users> findByEmailIgnoreCase(String username);

	public List<Users> findByRollnoIgnoreCase(String username);
}
