package com.college.clubs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.college.clubs.model.Events;

@Repository
public interface EventsRepo extends JpaRepository<Events, Integer> {

	@Query("select e from Events e where lower(e.description) like :search or "
			+ "lower(e.eventName) like :search or e.eventDate like :search")
	public List<Events> searchEvents(@Param("search") String search);

	@Query("select e from Events e where e.club.leader.id=:leaderId ")
	public List<Events> getByLeader(@Param("leaderId") Integer leaderId);

	@Query("select e from Events e where e.club.id in :clubIds ")
	public List<Events> getByClubIds(@Param("clubIds") List<Integer> clubIds);
}
