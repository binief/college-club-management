package com.college.clubs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.college.clubs.model.AttendanceHdr;
import com.college.clubs.model.Events;

@Repository
public interface AttendanceHdrRepo
		extends JpaRepository<AttendanceHdr, Integer> {

	public List<AttendanceHdr> findByEvent(Events e);

	@Query("select a from AttendanceHdr a where a.event.club.id= :clubId")
	public List<AttendanceHdr> findByClubId(@Param("clubId") Integer clubId);

	@Query("select a from AttendanceHdr a where a.event.club.staff.id=:staffId or a.event.club.staff2.id=:staffId ")
	public List<AttendanceHdr> findByStaffId(@Param("staffId") Integer staffId);

}
