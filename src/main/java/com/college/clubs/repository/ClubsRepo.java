package com.college.clubs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.college.clubs.model.Clubs;

@Repository
public interface ClubsRepo extends JpaRepository<Clubs, Integer> {

	@Query("select e from Clubs e where lower(e.clubName) like :search ")
	public List<Clubs> searchByClubNameIgnoreCase(
			@Param("search") String search);

	@Query("select e from Clubs e where e.id not in :ids ")
	public List<Clubs> findClubsNotInIds(@Param("ids") List<Integer> ids);

	@Query("select e from Clubs e where e.leader.id = :leaderId ")
	public List<Clubs> getClubByLeader(@Param("leaderId") Integer leaderId);

	@Query("select e from Clubs e where e.staff.id = :staffId ")
	public List<Clubs> getClubByStaff(@Param("staffId") Integer staffId);

	@Query("select e from Clubs e where e.staff2.id = :staffId ")
	public List<Clubs> getClubBySecondStaff(@Param("staffId") Integer staffId);

	@Query("select e from Clubs e where e.id in :ids ")
	public List<Clubs> findClubsInIds(@Param("ids") List<Integer> ids);

	public List<Clubs> findByClubNameIgnoreCase(String clubName);
}
