package com.college.clubs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.college.clubs.model.AttendanceDtl;

@Repository
public interface AttendanceDtlRepo
		extends JpaRepository<AttendanceDtl, Integer> {

	@Query("select a from AttendanceDtl a where a.attHdr.id= :hdrId ")
	public List<AttendanceDtl> getByHdrId(@Param("hdrId") Integer hdrId);
	
	@Query("select a from AttendanceDtl a where a.attHdr.id= :hdrId and a.present='P'")
	public List<AttendanceDtl> getPresentByHdrId(@Param("hdrId") Integer hdrId);

	@Query("select a from AttendanceDtl a where a.member.id= :userId and a.attHdr.id= :hdrId")
	public List<AttendanceDtl> getByMemberIdAndClubId(
			@Param("userId") Integer userId, @Param("hdrId") Integer hdrId);
}
