package com.college.clubs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.college.clubs.model.MemberClubMapping;

@Repository
public interface MemberClubMappingRepo
		extends JpaRepository<MemberClubMapping, Integer> {

	@Query("select m from MemberClubMapping m where m.member.id=:userId")
	public List<MemberClubMapping> getMyClubs(@Param("userId") Integer userId);
	
	@Query("select m from MemberClubMapping m where m.club.id=:clubId")
	public List<MemberClubMapping> getMembersByClub(@Param("clubId") Integer clubId);
	
}
