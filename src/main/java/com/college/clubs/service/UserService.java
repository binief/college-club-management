package com.college.clubs.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.Users;
import com.college.clubs.repository.UsersRepo;

@Service
public class UserService {

	@Autowired
	UsersRepo userRepo;

	@Autowired
	MailService mail;

	public Users validUser(String username, String password) {

		List<Users> users = userRepo.findByUsernameAndPassword(username,
				password);

		if (!users.isEmpty()) {
			return users.get(0);
		}

		return null;
	}

	public List<Users> findAll() {
		return userRepo.findAll();
	}

	public Users findById(Integer id) {
		Optional<Users> user = userRepo.findById(id);
		if (user.isEmpty()) {
			return null;
		}

		return user.get();
	}

	public Users saveUser(Users u) throws Exception {
		boolean newUser = u.getId() == null;

		u = userRepo.save(u);

		if (newUser) {
			String subject = "Account Created";
			String msg = "Dear " + u.getFullname() + ", \n"
					+ "You have been given access to MES Club.\n"
					+ "Please find your login details here.\n" + "Username: "
					+ u.getUsername() + "\n" + "Password: " + u.getPassword()
					+ "\n" + "Thank You";
			mail.sendMail(subject, msg, u.getEmail());
		}
		return u;
	}

	public String isDuplicateUser(Users u, boolean newUser) {

		if (newUser) {
			List<Users> users = userRepo
					.findByUsernameIgnoreCase(u.getUsername());
			if (!users.isEmpty())
				return "This username is already taken. Please user another username !!!";

			users = userRepo.findByFullnameIgnoreCase(u.getFullname());
			if (!users.isEmpty())
				return "This name is already taken. Please use another name !!!";

			users = userRepo.findByEmailIgnoreCase(u.getEmail());
			if (!users.isEmpty())
				return "This email id is already taken. Please use another email id !!!";

			if (u.getRollno() != null) {
				users = userRepo.findByRollnoIgnoreCase(u.getRollno());
				if (!users.isEmpty())
					return "This roll number is already taken. Please use another roll number !!!";
			}
		} else {
			List<Users> users = userRepo
					.findByUsernameIgnoreCase(u.getUsername());
			for (Users i : users) {
				if (!i.getId().equals(u.getId()))
					return "This username is already taken. Please user another username !!!";
			}

			users = userRepo.findByFullnameIgnoreCase(u.getFullname());
			for (Users i : users) {
				if (!i.getId().equals(u.getId()))
					return "This name is already taken. Please use another name !!!";
			}

			users = userRepo.findByEmailIgnoreCase(u.getEmail());
			for (Users i : users) {
				if (!i.getId().equals(u.getId()))
					return "This email id is already taken. Please use another email id !!!";
			}

			if (u.getRollno() != null) {
				users = userRepo.findByRollnoIgnoreCase(u.getRollno());
				for (Users i : users) {
					if (!i.getId().equals(u.getId()))
						return "This roll number is already taken. Please use another roll number !!!";
				}
			}
		}

		return null;
	}

	public void deleteUser(Users u) {
		userRepo.delete(u);
	}

	public List<Users> searchUser(String searchString) {
		return userRepo.findByFullnameIgnoreCase(searchString);
	}

	public List<Users> getClubMembers(Clubs club) {
		return userRepo.getClubMembers(club);
	}

	public List<Users> findByRole(String role) {
		return userRepo.findByRole(role);
	}

	public List<Users> findByUsername(String username) {
		return userRepo.findByUsernameIgnoreCase(username);
	}

}
