package com.college.clubs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.clubs.model.Clubs;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.model.Users;
import com.college.clubs.repository.ClubsRepo;
import com.college.clubs.repository.MemberClubMappingRepo;
import com.college.clubs.repository.UsersRepo;

@Service
public class ClubService {

	@Autowired
	ClubsRepo clubsRepo;

	@Autowired
	MemberClubMappingRepo memberRepo;

	@Autowired
	UsersRepo userRepo;

	@Autowired
	MailService mail;

	public List<Clubs> findAll() {
		return clubsRepo.findAll();
	}

	public Clubs findById(Integer id) {
		Optional<Clubs> club = clubsRepo.findById(id);
		if (club.isEmpty()) {
			return null;
		}

		return club.get();
	}

	public Clubs saveClub(Clubs u) {
		return clubsRepo.save(u);
	}

	public void deleteClub(Clubs u) {
		clubsRepo.delete(u);
	}

	public List<Clubs> doSearch(String searchString) {
		return clubsRepo.searchByClubNameIgnoreCase("%" + searchString + "%");
	}

	public List<Clubs> findClubsNotInIds(List<Integer> ids) {
		return clubsRepo.findClubsNotInIds(ids);
	}

	public List<Clubs> getClubByLeader(Integer leaderId) {
		return clubsRepo.getClubByLeader(leaderId);
	}

	public List<Clubs> getClubByStaff(Integer staffId) {
		List<Clubs> clubs = new ArrayList<Clubs>();
		List<Clubs> firstStaffClubs = clubsRepo.getClubByStaff(staffId);
		List<Clubs> secondStaffClubs = clubsRepo.getClubBySecondStaff(staffId);
		clubs.addAll(firstStaffClubs);
		clubs.addAll(secondStaffClubs);
		return clubs;
	}

	public List<Clubs> getByStudent(Integer studentId) {
		List<MemberClubMapping> memberClubMapping = memberRepo
				.getMyClubs(studentId);
		List<Integer> clubIds = memberClubMapping.stream()
				.map(MemberClubMapping::getClubId).collect(Collectors.toList());

		List<Clubs> clubs = clubsRepo.findClubsInIds(clubIds);

		return clubs;
	}

	public List<Clubs> findByClubNameIgnoreCase(String name) {
		return clubsRepo.searchByClubNameIgnoreCase(name);
	}

	public void validate(Clubs u) throws Exception {
		if (u.getId() == null) {
			List<Clubs> clubs = clubsRepo
					.findByClubNameIgnoreCase(u.getClubName());
			if (!clubs.isEmpty())
				throw new Exception("This club is already created !!!");
		} else {
			List<Clubs> clubs = clubsRepo
					.findByClubNameIgnoreCase(u.getClubName());
			for (Clubs c : clubs) {
				if (!c.getId().equals(u.getId()) && c.getClubName()
						.equalsIgnoreCase(u.getClubName().toLowerCase()))
					throw new Exception("This club is already created !!!");
			}
		}
	}

	@Transactional
	public void makeLeader(Users u, Clubs c, Users oldLeader) throws Exception {
		u.setRole("LEADER");
		userRepo.save(u);

		c.setLeader(u);
		clubsRepo.save(c);

		if (oldLeader != null) {
			oldLeader.setRole("MEMBER");
			userRepo.save(oldLeader);
		}

		String subject = "Leader Assigned";
		String msg = "Dear " + u.getFullname() + ", \n"
				+ "You have been assigned as leader for "
				+ c.getClubName() + "\n" + "Thank You";
		mail.sendMail(subject, msg, u.getEmail());
	}

}
