package com.college.clubs.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.clubs.model.AttendanceDtl;
import com.college.clubs.model.AttendanceHdr;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.model.Users;
import com.college.clubs.repository.AttendanceDtlRepo;
import com.college.clubs.repository.AttendanceHdrRepo;
import com.college.clubs.repository.MemberClubMappingRepo;

@Service
public class MemberClubMappingService {

	@Autowired
	MemberClubMappingRepo mappingRepo;

	@Autowired
	AttendanceDtlRepo attDtlRepo;

	@Autowired
	AttendanceHdrRepo attHdrRepo;

	@Autowired
	MailService mail;

	public List<MemberClubMapping> getMyClubs(Integer userId) {
		return mappingRepo.getMyClubs(userId);
	}

	@Transactional
	public MemberClubMapping becomeMember(MemberClubMapping m) {
		List<AttendanceHdr> attHdr = attHdrRepo.findByClubId(m.getClubId());
		if (!attHdr.isEmpty()) {
			AttendanceDtl attDtl = new AttendanceDtl();
			attDtl.setAttHdr(attHdr.get(0));
			attDtl.setMember(m.getMember());
			attDtlRepo.save(attDtl);
		}

		MemberClubMapping map = mappingRepo.save(m);
		if (m.getClub() != null)
			sendJoiningMail(m.getClub().getLeader(), m);
		return map;
	}

	@Transactional
	public boolean leaveMember(MemberClubMapping m) throws Exception {
		List<AttendanceHdr> attHdr = attHdrRepo.findByClubId(m.getClubId());
		if (attHdr != null && !attHdr.isEmpty()) {
			List<AttendanceDtl> attDtl = attDtlRepo.getByMemberIdAndClubId(
					m.getMemberId(), attHdr.get(0).getId());
			if (attDtl != null && !attDtl.isEmpty()) {
				List<AttendanceDtl> alreadyMarked = attDtl.stream()
						.filter(i -> i.getPresent() != null)
						.collect(Collectors.toList());
				if (alreadyMarked.size() > 0) {
					throw new Exception(
							"Cannot leave membership. Your attendance is already marked");
				}
				attDtlRepo.deleteAll(attDtl);
			}
		}
		mappingRepo.delete(m);
		return true;
	}

	public List<MemberClubMapping> getMembersByClub(Integer clubId) {
		return mappingRepo.getMembersByClub(clubId);
	}

	private void sendJoiningMail(Users leader, MemberClubMapping m) {
		try {
			if (leader != null) {
				String subject = "New Club Member !!!";
				String msg = "Dear " + leader.getFullname() + ", \n"
						+ "A new member added to club "
						+ m.getClub().getClubName() + " .\n" + "Member Name: "
						+ m.getMember().getFullname() + ".\n" + "Thank You";
				mail.sendMail(subject, msg, leader.getEmail());
			}
		} catch (Exception e) {

		}
	}
}
