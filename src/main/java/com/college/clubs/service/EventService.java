package com.college.clubs.service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.college.clubs.model.AttendanceDtl;
import com.college.clubs.model.AttendanceHdr;
import com.college.clubs.model.Events;
import com.college.clubs.model.MemberClubMapping;
import com.college.clubs.model.Users;
import com.college.clubs.repository.AttendanceDtlRepo;
import com.college.clubs.repository.AttendanceHdrRepo;
import com.college.clubs.repository.EventsRepo;
import com.college.clubs.repository.MemberClubMappingRepo;
import com.college.clubs.repository.UsersRepo;

@Service
public class EventService {
	@Autowired
	EventsRepo eventsRepo;

	@Autowired
	AttendanceHdrRepo attRepo;

	@Autowired
	AttendanceDtlRepo attDtlRepo;

	@Autowired
	MemberClubMappingRepo memberRepo;

	@Autowired
	UsersRepo userRepo;

	@Autowired
	MailService mail;

	public List<Events> findAll() {
		return eventsRepo.findAll();
	}

	public Events findById(Integer id) {
		Optional<Events> event = eventsRepo.findById(id);
		if (event.isEmpty()) {
			return null;
		}

		return event.get();
	}

	@Transactional
	public Events saveEvent(Events u) {
		if (u.getId() == null) {
			AttendanceHdr att = new AttendanceHdr();
			att.setAttendance(u.getEventDate());
			att.setEvent(u);
			att = attRepo.save(att);

			if (u.getClub() != null) {
				List<MemberClubMapping> clubs = memberRepo
						.getMembersByClub(u.getClubId());

				List<Users> users = clubs.stream()
						.map(MemberClubMapping::getMember)
						.collect(Collectors.toList());

				for (Users e : users) {
					AttendanceDtl dtl = new AttendanceDtl();
					dtl.setAttHdr(att);
					dtl.setMember(e);
					dtl = attDtlRepo.save(dtl);

					sendInvitationMail(e, u);
				}
			}
		} else {
			List<AttendanceHdr> atts = attRepo.findByEvent(u);

			if (!atts.isEmpty()) {
				AttendanceHdr att = atts.get(0);
				att.setAttendance(u.getEventDate());
				att.setEvent(u);
				attRepo.save(att);
			}
		}
		return eventsRepo.save(u);
	}

	public void deleteEvent(Events u) {
		eventsRepo.delete(u);
	}

	public List<Events> doSearch(String searchString) {
		return eventsRepo.searchEvents("%" + searchString + "%");
	}

	private void sendInvitationMail(Users e, Events u) {
		try {
			String pattern = "dd-MM-yyyy h:mm a";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

			String subject = "New Event";
			String msg = "Dear " + e.getFullname() + ", \n"
					+ "You are been invited into " + u.getEventName() + ".\n"
					+ (u.getVenue() != null
							? "This event is taking place at " + u.getVenue()
									+ " on "
									+ simpleDateFormat.format(u.getEventDate())
							: "")
					+ "\nPlease login for more details.\n" + "Thank You";
			
			System.out.println(msg);
			mail.sendMail(subject, msg, e.getEmail());
		} catch (Exception ignore) {
		}
	}

	public List<Events> getByLeader(Integer leaderId) {
		return eventsRepo.getByLeader(leaderId);
	}

	public List<Events> getByStudent(Integer studentId) {
		List<MemberClubMapping> memberClubMapping = memberRepo
				.getMyClubs(studentId);
		List<Integer> clubIds = memberClubMapping.stream()
				.map(MemberClubMapping::getClubId).collect(Collectors.toList());

		List<Events> events = eventsRepo.getByClubIds(clubIds);

		return events;
	}
}
