package com.college.clubs.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.college.clubs.model.AttendanceDtl;
import com.college.clubs.model.AttendanceHdr;
import com.college.clubs.model.Events;
import com.college.clubs.model.Users;
import com.college.clubs.repository.AttendanceDtlRepo;
import com.college.clubs.repository.AttendanceHdrRepo;

@Service
public class AttendanceService {

	@Autowired
	AttendanceHdrRepo attRepo;

	@Autowired
	AttendanceDtlRepo attDtlRepo;

	@Autowired
	UserService userService;

	public void createAttendance(Events e) {
		try {
			AttendanceHdr att = new AttendanceHdr();
			att.setAttendance(e.getEventDate());
			att.setEvent(e);
			att = attRepo.save(att);

			if (e.getClub() != null) {
				List<Users> users = userService.getClubMembers(e.getClub());
				for (Users u : users) {
					AttendanceDtl dtl = new AttendanceDtl();
					dtl.setAttHdr(att);
					dtl.setMember(u);
					dtl.setPresent("A");
					dtl = attDtlRepo.save(dtl);
				}
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public void updateAttendance(Events e) {
		try {
			List<AttendanceHdr> atts = attRepo.findByEvent(e);

			if (!atts.isEmpty()) {
				AttendanceHdr att = atts.get(0);
				att.setAttendance(e.getEventDate());
				att.setEvent(e);
				attRepo.save(att);
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public List<AttendanceHdr> getAllAttHdr() {
		try {

			List<AttendanceHdr> atts = attRepo
					.findAll(Sort.by("attendance").descending());

			return atts;
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return null;
	}

	public AttendanceHdr getById(Integer id) {
		try {
			Optional<AttendanceHdr> atts = attRepo.findById(id);

			if (!atts.isEmpty())
				return atts.get();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return null;
	}

	public List<AttendanceDtl> getDetailByHdrId(Integer id) {
		try {
			return attDtlRepo.getByHdrId(id);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return null;
	}

	public List<AttendanceDtl> getPresentDetailByHdrId(Integer id) {
		try {
			return attDtlRepo.getPresentByHdrId(id);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return null;
	}

	public AttendanceDtl saveDtl(AttendanceDtl dtl) {
		dtl = attDtlRepo.save(dtl);
		return dtl;
	}

	public AttendanceHdr saveHdr(AttendanceHdr hdr) {
		hdr = attRepo.save(hdr);
		return hdr;
	}
	
	public List<AttendanceHdr> findByStaffId(@Param("staffId") Integer staffId){
		return attRepo.findByStaffId(staffId);
	}
}
