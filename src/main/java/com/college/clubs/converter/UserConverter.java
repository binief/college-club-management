package com.college.clubs.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.college.clubs.controller.ClubController;

//@Named(value = "userConverter")
@FacesConverter("userConverter")
public class UserConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) throws ConverterException {
		/*
		 * if (value == null) return null; Users u = new Users();
		 * u.setId(Integer.valueOf(value)); return u;
		 */
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
                        .createValueExpression(context.getELContext(),
                                "#{club}", ClubController.class);

		ClubController ctrl = (ClubController)vex.getValue(context.getELContext());
		ctrl.getU().getLeader();
        return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) throws ConverterException {
		// TODO Auto-generated method stub
		return null;
	}

}
