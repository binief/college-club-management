package com.college.clubs.model;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "events")
public class Events {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "eventname")
	private String eventName;

	@Column(name = "eventdate")
	private Date eventDate;

	@Column(name = "description")
	private String description;

	@ManyToOne
	@JoinColumn(name = "clubid")
	private Clubs club;

	@Column(name = "venue")
	private String venue;

	@Transient
	private Integer clubId;

	@Transient
	private String status;

	public Integer getClubId() {
		if (this.getClub() != null)
			this.clubId = this.getClub().getId();
		return clubId;
	}

	public void setClubId(Integer clubId) {
		this.clubId = clubId;
		if (this.clubId != null) {
			Clubs u = new Clubs();
			u.setId(clubId);
			this.setClub(u);
		} else {
			this.setClub(null);
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Clubs getClub() {
		return club;
	}

	public void setClub(Clubs club) {
		this.club = club;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStatus() {
		if (this.getEventDate() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("GMT"));

			if (this.getEventDate().after(cal.getTime())) {
				return "Pending";
			}
		}
		return "Completed";
	}

	public void setStatus(Boolean status) {
		if (this.getEventDate() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("GMT"));

			if (this.getEventDate().after(cal.getTime())) {
				this.status = "Pending";
			}
		}
		this.status = "Completed";
	}

}
