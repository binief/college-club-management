package com.college.clubs.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "attendancehdr")
public class AttendanceHdr {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "attendance")
	private Date attendance;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "eventid")
	private Events event;

	@Column(name = "approved")
	private String approved;

	@Transient
	private Integer eventId;

	public Integer getEventId() {
		if (this.getEvent() != null)
			this.eventId = this.getEvent().getId();
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
		if (this.eventId != null) {
			Events u = new Events();
			u.setId(eventId);
			this.setEvent(u);
		} else {
			this.setEvent(null);
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getAttendance() {
		return attendance;
	}

	public void setAttendance(Date attendance) {
		this.attendance = attendance;
	}

	public Events getEvent() {
		return event;
	}

	public void setEvent(Events event) {
		this.event = event;
	}

	public String getApproved() {
		return approved;
	}

	public void setApproved(String approved) {
		this.approved = approved;
	}

}
