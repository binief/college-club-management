package com.college.clubs.model;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "users")
public class Users {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "fullname")
	private String fullname;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "department")
	private String department;

	@Column(name = "email")
	private String email;

	@Column(name = "contactno")
	private String contactno;

	@Column(name = "role")
	private String role;

	@ManyToOne
	@JoinColumn(name = "clubid")
	private Clubs club;

	@JoinColumn(name = "dateofadmission")
	private Date dateofadmission;

	@JoinColumn(name = "rollno")
	private String rollno;

	@Transient
	private Boolean status;

	@ManyToOne
	@JoinColumn(name = "studentid")
	private Users student;

	@Column(name = "program")
	private String program;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Clubs getClub() {
		return club;
	}

	public void setClub(Clubs club) {
		this.club = club;
	}

	public Date getDateofadmission() {
		return dateofadmission;
	}

	public void setDateofadmission(Date dateOfAdmission) {
		this.dateofadmission = dateOfAdmission;
	}

	public String getRollno() {
		return rollno;
	}

	public void setRollno(String rollNo) {
		this.rollno = rollNo;
	}

	public Boolean getStatus() {
		if (this.getDateofadmission() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("GMT"));
			cal.add(Calendar.YEAR, -2);

			if (this.getDateofadmission().before(cal.getTime())) {
				return false;
			}
		}
		return true;
	}

	public void setStatus(Boolean status) {
		if (this.getDateofadmission() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("GMT"));
			cal.add(Calendar.YEAR, -2);

			if (this.getDateofadmission().before(cal.getTime())) {
				this.status = false;
			}
		}
		this.status = true;
	}

	public Users getStudent() {
		return student;
	}

	public void setStudent(Users student) {
		this.student = student;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", fullname=" + fullname + ", username="
				+ username + ", password=" + password + ", department="
				+ department + ", email=" + email + ", contactno=" + contactno
				+ ", role=" + role + ", club=" + club + ", dateofadmission="
				+ dateofadmission + ", rollno=" + rollno + ", status=" + status
				+ ", student=" + student + ", program=" + program + "]";
	}

}
