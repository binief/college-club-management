package com.college.clubs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "clubs")
public class Clubs {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "clubname")
	private String clubName;

	@ManyToOne
	@JoinColumn(name = "leaderid")
	private Users leader;

	@ManyToOne
	@JoinColumn(name = "staffid")
	private Users staff;

	@ManyToOne
	@JoinColumn(name = "staff2id")
	private Users staff2;

	@Transient
	private Integer leaderId;

	@Transient
	private Integer staffId;

	@Transient
	private Integer staff2Id;

	public Integer getLeaderId() {
		if (this.getLeader() != null)
			this.leaderId = this.getLeader().getId();
		return leaderId;
	}

	public void setLeaderId(Integer leaderId) {
		this.leaderId = leaderId;
		if (this.leaderId != null) {
			Users u = new Users();
			u.setId(leaderId);
			this.setLeader(u);
		} else {
			this.setLeader(null);
		}
	}

	public Integer getStaffId() {
		if (this.getStaff() != null)
			this.staffId = this.getStaff().getId();
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
		if (this.staffId != null) {
			Users u = new Users();
			u.setId(staffId);
			this.setStaff(u);
		} else {
			this.setStaff(null);
		}
	}

	public Integer getStaff2Id() {
		if (this.getStaff2() != null)
			this.staff2Id = this.getStaff2().getId();
		return staff2Id;
	}

	public void setStaff2Id(Integer staff2Id) {
		this.staff2Id = staff2Id;
		if (this.staff2Id != null) {
			Users u = new Users();
			u.setId(staff2Id);
			this.setStaff2(u);
		} else {
			this.setStaff2(null);
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getClubName() {
		return clubName;
	}

	public void setClubName(String clubName) {
		this.clubName = clubName;
	}

	public Users getLeader() {
		return leader;
	}

	public void setLeader(Users leader) {
		this.leader = leader;
	}

	public Users getStaff() {
		return staff;
	}

	public void setStaff(Users staff) {
		this.staff = staff;
	}

	public Users getStaff2() {
		return staff2;
	}

	public void setStaff2(Users staff2) {
		this.staff2 = staff2;
	}

}
