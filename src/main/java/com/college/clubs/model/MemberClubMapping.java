package com.college.clubs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "memberclubmapping")
public class MemberClubMapping {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "clubid")
	private Clubs club;

	@ManyToOne
	@JoinColumn(name = "memberid")
	private Users member;

	@Transient
	private Integer clubId;

	@Transient
	private Integer memberId;

	public Integer getClubId() {
		if (this.getClub() != null)
			this.clubId = this.getClub().getId();
		return clubId;
	}

	public void setClubId(Integer clubId) {
		this.clubId = clubId;
		if (this.clubId != null) {
			Clubs u = new Clubs();
			u.setId(clubId);
			this.setClub(u);
		} else {
			this.setClub(null);
		}
	}

	public Integer getMemberId() {
		if (this.getMember() != null)
			this.memberId = this.getMember().getId();
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
		if (this.memberId != null) {
			Users u = new Users();
			u.setId(memberId);
			this.setMember(u);
		} else {
			this.setMember(null);
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Clubs getClub() {
		return club;
	}

	public void setClub(Clubs club) {
		this.club = club;
	}

	public Users getMember() {
		return member;
	}

	public void setMember(Users member) {
		this.member = member;
	}

}
