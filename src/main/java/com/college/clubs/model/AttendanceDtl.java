package com.college.clubs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "attendancedtl")
public class AttendanceDtl {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "memberid")
	private Users member;

	@ManyToOne
	@JoinColumn(name = "atthdrid")
	private AttendanceHdr attHdr;

	@Column(name = "present")
	private String present;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Users getMember() {
		return member;
	}

	public void setMember(Users member) {
		this.member = member;
	}

	public AttendanceHdr getAttHdr() {
		return attHdr;
	}

	public void setAttHdr(AttendanceHdr attHdr) {
		this.attHdr = attHdr;
	}

	public String getPresent() {
		return present;
	}

	public void setPresent(String present) {
		this.present = present;
	}

}
